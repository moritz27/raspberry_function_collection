#import numpy as np
#import pandas as pd
#import mplcursors
import matplotlib.pyplot as plt
import csv


logdata = {
 "longitude":[],
 "latitude":[],
 "rssi":[]
}

with open('logs/mapper_09_03_2021_18_26_14.csv', newline='\n') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        logdata["longitude"].append(float(row['longitude']))
        logdata["latitude"].append(float(row['latitude']))
        logdata["rssi"].append(int(row['rssi']))
        #print(float(row['N']), float(row['E']), int(row['rssi']))
        
#print(logdata)

bounding_box = {
    "longitude":{
        "min":min(logdata["longitude"]),
        "max":max(logdata["longitude"])
    },
    "latitude":{
        "min":min(logdata["latitude"]),
        "max":max(logdata["latitude"])
    },
    "rssi":{
        "min":min(logdata["rssi"]),
        "max":max(logdata["rssi"])
    }
}

print(bounding_box)

fig, ax = plt.subplots(figsize = (30, 10))
img = plt.imread("map.png")
ax.set_title('Plotting GPS Data on Map')
ax.imshow(img, extent=[10.96020, 10.97064, 50.25774, 50.26219])
#ax.imshow(img, extent=[bounding_box["latitude"]["min"], bounding_box["latitude"]["max"], bounding_box["longitude"]["min"], bounding_box["longitude"]["max"]])
cm = plt.cm.get_cmap('RdYlBu')
sc = plt.scatter(x=logdata["latitude"], y=logdata["longitude"], c=logdata["rssi"], vmin=(min(logdata["rssi"])), vmax=(max(logdata["rssi"])), s=35, cmap=cm)
plt.colorbar(sc)
#plt.ginput(4)
#mplcursors.cursor(hover=True)
plt.show()
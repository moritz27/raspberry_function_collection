# pip3 install adafruit-circuitpython-rfm9x

import time
import board
import busio
import digitalio
import RPi.GPIO as io
import adafruit_rfm9x
import struct 
from datetime import datetime


# setup interrupt callback function
def rfm9x_callback(rfm9x_irq):
    global received # pylint: disable=global-statement
    #print("IRQ detected ", rfm9x_irq, rfm9x.rx_done)
    # check to see if this was a rx interrupt - ignore tx
    if rfm9x.rx_done:
        packet = rfm9x.receive(timeout=None)
        if packet is not None:
            received["flag"] = 1
            received["data"] = packet#format(packet)
            received["rssi"] = format(rfm9x.last_rssi)
            
def convert_gps_data(data, precision = 6):
    try:
        coordinates = {
            "longitude":9999.999999,
            "latitude":9999.999999
        }
               
        #convert NMEA format to coordinates
        coordinates["longitude"] = int(data[0:2]) + round(((float(data[2:(5+precision)])  / 60.0)), precision)
        coordinates["latitude"] = int(data[(6+precision):(8+precision)]) + round(((float(data[(8+precision):(16+precision)]) / 60.0)), precision)

        print(coordinates["longitude"],",", coordinates["latitude"])
        
        return (coordinates)
    except:
        return
        
    
def create_file():
    if not os.path.exists("logs"):
        os.rmdir("logs")
    file_name_string = "logs/mapper_" + get_datetime() + ".csv"
    file = open(file_name_string, "a")
    file.write("longitude")
    file.write(',')
    file.write("latitude")
    file.write(',')
    file.write("rssi")
    file.write('\n')
    return file
    
    
def write_to_file(file, linedata, signalstrength):
    file.write(str(linedata["longitude"]))
    file.write(',')
    file.write(str(linedata["latitude"]))
    file.write(',')
    file.write(str(signalstrength))
    file.write('\n')
    
    
def get_datetime():
    # datetime object containing current date and time
    now = datetime.now()
    #print("now =", now)
    # dd/mm/YY H:M:S
    dt_string = now.strftime("%d_%m_%Y_%H_%M_%S")
    #print("date and time =", dt_string)
    return dt_string

# Define Radio Parameters
RADIO_FREQ_MHZ = 868.0  

# Define Pins
CS = digitalio.DigitalInOut(board.D25)
RESET = digitalio.DigitalInOut(board.D17)

# Initialize SPI Bus.
spi = busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO)

# Initialze RFM Radio
rfm9x = adafruit_rfm9x.RFM9x(spi, CS, RESET, RADIO_FREQ_MHZ)

# Set Output Power
rfm9x.tx_power = 5

# configure the interrupt pin and event handling.
RFM9X_G0 = 4
io.setmode(io.BCM)
io.setup(RFM9X_G0, io.IN, pull_up_down=io.PUD_DOWN)  # activate input
io.add_event_detect(RFM9X_G0, io.RISING)
io.add_event_callback(RFM9X_G0, rfm9x_callback)


received = {
    "flag":0,
    "data":'',
    "rssi":999
}


rfm9x.send(bytes("LoRa Mapper", "utf-8"), keep_listening=True)


try:
    logfile = create_file()
    print("Waiting for packets...")
    while True:
        time.sleep(0.1)
        if received["flag"]:
            print(received["data"], "with RSSI:", received["rssi"])
            coordinates = convert_gps_data(received["data"])
            if coordinates:
                write_to_file(logfile, coordinates, received["rssi"])
            #print("with RSSI:", received["rssi"])
            received["flag"] = 0
        
finally:
    logfile.close()

// Mohrenstrasse: 50.260492637663035, 10.963289145409695

#include <SPI.h>
#include <LoRa.h>
#include <Adafruit_GPS.h>
#include <SoftwareSerial.h>
//#include <Adafruit_SleepyDog.h>

#define txPower 20 // LoRa in dBm; max allowed: 14dBm

#define precision 6
SoftwareSerial mySerial(A5, A4); //GPS: TX, RX
Adafruit_GPS GPS(&mySerial);

#define GPSECHO  false
unsigned char StandbyMode[] = {"$PMTK161,0*28\x0D\x0A"};

void parse_GPS_data(){
  char c = GPS.read();
  if (GPS.newNMEAreceived()) {
    if (!GPS.parse(GPS.lastNMEA())) 
      return;  
  }
}

void print_NMEA_coordinates(){
  Serial.print("Location in NMEA Format: ");
  Serial.print(GPS.latitude, precision); Serial.print(GPS.lat);
  Serial.print(", ");
  Serial.print(GPS.longitude, precision); Serial.println(GPS.lon);
}

void print_degreee_coordinates(){
  //XXYY.ZZZZ ⇒ XX° + (YY.ZZZZ / 60)°
  Serial.print("Location in Degree Format: ");
  Serial.print(GPS.latitude, precision); 
  Serial.print(", ");
  Serial.print(GPS.longitude, precision);
}

void print_GPS_data(){
    Serial.print("\nTime: ");
    if (GPS.hour < 10) { Serial.print('0'); }
    Serial.print(GPS.hour, DEC); Serial.print(':');
    if (GPS.minute < 10) { Serial.print('0'); }
    Serial.print(GPS.minute, DEC); Serial.print(':');
    if (GPS.seconds < 10) { Serial.print('0'); }
    Serial.print(GPS.seconds, DEC); Serial.print('.');
    if (GPS.milliseconds < 10) {
      Serial.print("00");
    } else if (GPS.milliseconds > 9 && GPS.milliseconds < 100) {
      Serial.print("0");
    }
    Serial.println(GPS.milliseconds);
    Serial.print("Date: ");
    Serial.print(GPS.day, DEC); Serial.print('/');
    Serial.print(GPS.month, DEC); Serial.print("/20");
    Serial.println(GPS.year, DEC);
    Serial.print("Fix: "); Serial.print((int)GPS.fix);
    Serial.print(" quality: "); Serial.println((int)GPS.fixquality);
    if (GPS.fix) {
      print_NMEA_coordinates();
      Serial.print("Speed (knots): "); Serial.println(GPS.speed);
      Serial.print("Angle: "); Serial.println(GPS.angle);
      Serial.print("Altitude: "); Serial.println(GPS.altitude);
      Serial.print("Satellites: "); Serial.println((int)GPS.satellites);
    }
}

void send_GPS_coordinates(){
  Serial.println("Sending LoRa message");
  //LoRa.idle();
  LoRa.beginPacket();
  LoRa.print("XXXX"); //This part is lost
  LoRa.print(GPS.latitude, precision);// LoRa.print(GPS.lat);
  LoRa.print(",");
  LoRa.print(GPS.longitude, precision);// LoRa.println(GPS.lon);
  LoRa.endPacket();
  //LoRa.sleep();
}


void setup()
{
  Serial.begin(115200);
  delay(5000);
  Serial.println("LoRa GPS Mapper");

  if (!LoRa.begin(868E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
  LoRa.setTxPower(txPower);

  //LoRa.sleep();

  GPS.begin(9600);
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA); //RMC (recommended minimum) and GGA (fix data) including altitude
  //GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCONLY); //only the "minimum recommended" data
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);  // 1 Hz update rate
  GPS.sendCommand(PGCMD_ANTENNA); // Request updates on antenna status
  delay(1000);
  mySerial.println(PMTK_Q_RELEASE); // Ask for firmware version
}

uint32_t timer = millis();
void loop()      // run over and over again
{
  parse_GPS_data();
  if (millis() - timer > 2000) {
    //Serial.println(GPS.wakeup());
    //while (!GPS.fix) {
    //  parse_GPS_data();
    //}
    if (GPS.fix) {
      print_NMEA_coordinates();
      //print_GPS_data();
      send_GPS_coordinates();
    }
    timer = millis(); // reset the timer
    //Serial.println(GPS.standby());
    //delay(5000);
    //GPS.pause(true);
    //GPS.write(StandbyMode);
    //char c = GPS.read();
    //Serial.write(c);
  }

  //Serial.println("Going to sleep now..");
  //Serial.println("");
  //delay(100);
  //Watchdog.sleep(5000);
  
}

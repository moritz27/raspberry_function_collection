| Device Pin | Name |    Remarks   | RPi Pin |  RPi Function  |
|:----------:|:----:|:------------:|:-------:|:--------------:|
| 1          | VCC  | +3.3V Power  | P01-17  | 3V3            |
| 2          | GND  | Ground       | P01-20  | GND            |
| 3          | D0   | Clock        | P01-23  | GPIO 11 (SCLK) |
| 4          | D1   | MOSI         | P01-19  | GPIO 10 (MOSI) |
| 5          | RST  | Reset        | P01-22  | GPIO 25        |
| 6          | DC   | Data/Command | P01-18  | GPIO 24        |
| 7          | CS   | Chip Select  | P01-24  | GPIO 8 (CE0)   |
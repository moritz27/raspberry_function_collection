#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2014-2020 Richard Hull and contributors
# See LICENSE.rst for details.
# PYTHON_ARGCOMPLETE_OK


import os
import sys
import time
import socket
import fcntl
import struct
from pathlib import Path
from datetime import datetime

if os.name != 'posix':
    sys.exit('{} platform not supported'.format(os.name))

from demo_opts import get_device
from luma.core.render import canvas
from PIL import ImageFont

try:
    import psutil
except ImportError:
    print("The psutil library was not found. Run 'sudo -H pip install psutil' to install it.")
    sys.exit()


# TODO: custom font bitmaps for up/down arrows
# TODO: Load histogram


def bytes2human(n):
    """
    >>> bytes2human(10000)
    '9K'
    >>> bytes2human(100001221)
    '95M'
    """
    symbols = ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
    prefix = {}
    for i, s in enumerate(symbols):
        prefix[s] = 1 << (i + 1) * 10
    for s in reversed(symbols):
        if n >= prefix[s]:
            value = int(float(n) / prefix[s])
            return '%s%s' % (value, s)
    return "%sB" % n


def cpu_usage():
    # load average, uptime
    uptime = datetime.now() - datetime.fromtimestamp(psutil.boot_time())
    av1, av2, av3 = os.getloadavg()
    return "CPU: %.1f  Up: %s" % (av1, str(uptime).split('.')[0])


def mem_usage():
    usage = psutil.virtual_memory()
    return "Mem: %s of %s %.0f%%" % (bytes2human(usage.used), bytes2human(usage.total), usage.percent)


def disk_usage(dir):
    usage = psutil.disk_usage(dir)
    return "SD: %s %.0f%%" % (bytes2human(usage.used), usage.percent)

def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', bytes(ifname[:15], 'utf-8'))
    )[20:24])
    
def get_CPU_temp():
    CPU_temp = float(os.popen('cat /sys/class/thermal/thermal_zone0/temp').readline())/1000
    return round(CPU_temp,1)

def get_GPU_temp():
    res = os.popen('vcgencmd measure_temp').readline()
    GPU_temp = float((res.replace("temp=","").replace("'C\n","")))
    return round(GPU_temp,1)
    
def get_all_temps():
    temperatures = {}
    temperatures["CPU"] = get_CPU_temp()
    temperatures["GPU"] = get_CPU_temp()
    return temperatures
    
def calc_temp_mean(temperatures):
    temp_sum = 0
    for temp in temperatures:
        temp_sum += temperatures[temp]
    return "Temp: %s °C" % (round((temp_sum / len(temperatures)),1))


def network(iface):
    #hostname = socket.gethostname()
    #local_ip = str(socket.gethostbyname(hostname))
    local_ip = str(get_ip_address(iface))
    #print(local_ip)
    return "%s IP: %s" % (iface, local_ip)
    #stat = psutil.net_io_counters(pernic=True)[iface]
    #return "%s: Tx%s, Rx%s" % \
    #       (iface, bytes2human(stat.bytes_sent), bytes2human(stat.bytes_recv))


def stats(device):
    # use custom font
    font_path = str(Path(__file__).resolve().parent.joinpath('fonts', 'C&C Red Alert [INET].ttf'))
    font2 = ImageFont.truetype(font_path, 12)
    font3 = ImageFont.truetype(font_path, 18)

    with canvas(device) as draw:
        y = 0
        draw.text((0, y), "SDR Server", font=font3, fill="white")
        y += 18
        draw.text((0, y), cpu_usage(), font=font2, fill="white")
        y +=12
        draw.text((0, y), calc_temp_mean(get_all_temps()), font=font2, fill="white")
        y +=12
        #draw.text((0, y), mem_usage(), font=font2, fill="white")
        #y +=12
        #draw.text((0, y), disk_usage('/'), font=font2, fill="white")
        try:
            draw.text((0, y), network('eth0'), font=font2, fill="white")
            y +=12
        except OSError:
            # no ethernet enabled/available
            pass
        try:
            draw.text((0, y), network('wlan0'), font=font2, fill="white")
            y +=12
        except OSError:
            # no wifi enabled/available
            pass


def main():
    while True:
        stats(device)
        time.sleep(1)


if __name__ == "__main__":
    try:
        device = get_device()
        main()
    except KeyboardInterrupt:
        pass

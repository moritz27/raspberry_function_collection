#! /usr/bin/env python
#import os
import RPi.GPIO as GPIO #install: sudo apt-get install python3-rpi.gpio
import time
from subprocess import call

led_pin = 5
button_pin = 21


sleepTime1=0.5
sleepTime2=0.1

GPIO.setmode(GPIO.BCM)
GPIO.setup(button_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP) #Button
GPIO.setup(led_pin, GPIO.OUT) #LED

print("Starting")

try:
    for i in range (0,5):
        GPIO.output(led_pin, GPIO.HIGH)
        time.sleep(sleepTime1)
        GPIO.output(led_pin, GPIO.LOW)
        time.sleep(sleepTime1)
    GPIO.output(led_pin, GPIO.HIGH)
    GPIO.wait_for_edge(button_pin, GPIO.FALLING)
    print("Shutting Down now")
    for i in range (0,5):
        GPIO.output(led_pin, GPIO.HIGH)
        time.sleep(sleepTime2)
        GPIO.output(led_pin, GPIO.LOW)
        time.sleep(sleepTime2)
    GPIO.output(led_pin, GPIO.HIGH)
    # shut down the rpi
    call("sudo poweroff", shell=True)
    #os.system("/sbin/shutdown -h now")
except:
    print("Exception occured")
    GPIO.cleanup()
    
finally:
    GPIO.cleanup()

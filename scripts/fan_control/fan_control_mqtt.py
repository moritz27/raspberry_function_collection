#!/usr/bin/env python3
import os
from time import sleep
import signal
import sys
import json
import RPi.GPIO as GPIO #install: sudo apt-get install python3-rpi.gpio
import paho.mqtt.publish as publish #install: sudo apt-get install python3-pip && pip3 install paho-mqtt

MQTT_SERVER = "192.168.179.11" #your mqtt server ip
MQTT_PATH = "ender_ps_temp" #your mqtt topic

pin = 18
max_temperature = 45
hysteresis = 2

def setup():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin, GPIO.OUT)
    GPIO.setwarnings(False)
    sleep(1)
    return()

def get_CPU_temp():
    CPU_temp = float(os.popen('cat /sys/class/thermal/thermal_zone0/temp').readline())/1000
    return CPU_temp

def get_GPU_temp():
    res = os.popen('vcgencmd measure_temp').readline()
    GPU_temp = float((res.replace("temp=","").replace("'C\n","")))
    return GPU_temp

def fanON():
    setPin(True)
    return()

def fanOFF():
    setPin(False)
    return()

def getTEMP():
    temperatures = {}
    temperatures["CPU"] = get_CPU_temp()
    temperatures["GPU"] = get_CPU_temp()
    print("CPU Temp: ",temperatures["CPU"], " GPU_temp: ", temperatures["GPU"])
    payload = json.dumps(temperatures)
    publish.single(MQTT_PATH, payload, hostname=MQTT_SERVER)
    #publish.single(MQTT_PATH, GPU_temp, hostname=MQTT_SERVER)
    if (temperatures["GPU"] > (max_temperature + hysteresis)) or (temperatures["CPU"] > (max_temperature + hysteresis)):
        fanON()
    elif (temperatures["GPU"] < (max_temperature - hysteresis)) and (temperatures["CPU"] < (max_temperature - hysteresis)):
        fanOFF()
    return()

def setPin(mode):
    GPIO.output(pin, mode)
    return()

try:
	sleep(30)
	setup()
	while True:
		getTEMP()
		sleep(5)

except KeyboardInterrupt:
    GPIO.cleanup()

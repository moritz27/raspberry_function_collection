import time
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(22, GPIO.OUT)

p = GPIO.PWM(22, 500)  # frequency=50Hz
p.start(0)
try:
	p.ChangeDutyCycle(80)
	while (1):
		pass
except KeyboardInterrupt:
	pass
	p.stop()
	GPIO.cleanup()
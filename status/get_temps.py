import os
from time import sleep

def get_CPU_temp():
    CPU_temp = float(os.popen('cat /sys/class/thermal/thermal_zone0/temp').readline())/1000
    return round(CPU_temp,1)

def get_GPU_temp():
    res = os.popen('vcgencmd measure_temp').readline()
    GPU_temp = float((res.replace("temp=","").replace("'C\n","")))
    return round(GPU_temp,1)
    
def get_all_temps():
    temperatures = {}
    temperatures["CPU"] = get_CPU_temp()
    temperatures["GPU"] = get_CPU_temp()
    return temperatures
    
def calc_temp_mean(temperatures):
    temp_sum = 0
    for temp in temperatures:
        temp_sum += temperatures[temp]
    return round((temp_sum / len(temperatures)),1)

while 1:
    print("Mean Temp:", calc_temp_mean(get_all_temps()))
    #print("CPU Temp:",temperatures["CPU"], " GPU_temp:", temperatures["GPU"], " Mean Temp:", calc_temp_mean(temperatures))
    sleep(1)   
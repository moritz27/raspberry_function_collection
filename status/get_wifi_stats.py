import socket
import fcntl
import struct

hostname = socket.gethostname()
local_ip = socket.gethostbyname(hostname)
print(local_ip)

def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', bytes(ifname[:15], 'utf-8'))
    )[20:24])

local_ip_2 = str(get_ip_address("wlan0"))
print(local_ip_2)

print(wlan.status('rssi'))
# pip3 install smbus2

from smbus2 import SMBusWrapper
address = 0x05
data = [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF]

def sendByteArray(data, address):
    with SMBusWrapper(1) as bus:
        bus.write_i2c_block_data(address, 1, data)
    return -1

sendByteArray(data, address)
#install: sudo apt-get install python3-pip && pip3 install paho-mqtt
import paho.mqtt.publish as publish 
MQTT_SERVER = "192.168.179.11" #your mqtt server ip
MQTT_PATH = "ender_ps_temp" #your mqtt topic

test_data =  { "Foo": "Bar" }
payload = json.dumps(test_data)
publish.single(MQTT_PATH, payload, hostname=MQTT_SERVER)